import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { AuthService } from '../services/auth.service';
import Swal from 'sweetalert2'

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor( public auth: AuthService, private router: Router ) { }

  ngOnInit(): void {

    const tag = document.createElement('script');
 
    tag.src = "https://www.youtube.com/iframe_api";
 
    document.body.appendChild(tag);

    this.auth.estaAutenticado();
        setInterval(() => { this.auth.estaAutenticado(); }, 10000);   

  }

  salir(){

    this.auth.logout();
    Swal.fire({
      icon: 'success',
      title: 'Cerraste sesión!',
      text: 'Se ha cerrado sesión exitosamente',
      timer: 1000000
    });
  }
  login(){

    Swal.fire({
      icon: 'success',
      title: 'Iniciar Sesión!',
      text: 'Se ha iniciado sesión exitosamente',
      timer: 1000000
    });
  }

  logup(){

    Swal.fire({
      icon: 'success',
      title: 'Registrarse!',
      text: 'Se ha registrado exitosamente',
      timer: 1000000
    });
  }

}
