import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';

import { UsuarioModel } from '../models/usuario.model';
import { AuthService } from '../services/auth.service';
import Swal from 'sweetalert2'

@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.css']
})
export class RegistroComponent implements OnInit {

  usuario: UsuarioModel;

  constructor(  private auth: AuthService, private router: Router ) { }

  ngOnInit(): void {

    this.usuario = new UsuarioModel();

  }

  registro( form: NgForm){

    if ( form.invalid ) {
      console.log('Formulario no valido');
      return;
    }

    Swal.fire({
      allowOutsideClick: false,
      icon: 'info',
      text: 'Espere por favor!'
    });
    Swal.showLoading();

    this.auth.nuevoUsuario( this.usuario )
      .subscribe( resp => {

        console.log(resp);
        Swal.close();
        this.router.navigateByUrl('/home');

        ///Registro datos de usuario
        this.auth.CrearUsuario( this.usuario )
          .subscribe( resp => {
            console.log(resp);
        });

      }, (err) => {
        console.log(err.error.error.message);
        
        Swal.fire({
          icon: 'error',
          title: 'Error al autenticar',
          text: err.error.error.message
        });
      });

    }

}
