import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { RegistroComponent } from './registro/registro.component';
import { HomeComponent } from './home/home.component';
import { PerfilComponent } from './perfil/perfil.component';
//import { CallbackComponent } from './callback/callback.component';

const routes: Routes = [

  { path: 'login', component: LoginComponent},
  { path: 'registro', component: RegistroComponent},
  { path: 'home', component: HomeComponent},
  { path: 'perfil', component: PerfilComponent},
  { path: 'home/:id', component: HomeComponent},
  { path: 'perfil/:id', component: PerfilComponent},
  //{ path: 'callback', component: CallbackComponent},
  { path: '**', redirectTo: 'home', pathMatch: 'full' }

];

export const AppRoutingModule = RouterModule.forRoot( routes, { useHash: true } );
