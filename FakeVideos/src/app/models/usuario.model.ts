export class UsuarioModel {

    id: string;
    email: string;
    password: string;
    password2: string;
    nombre: string;
    apellido: string;
    fecha_registro: Date;

}