import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { UsuarioModel } from '../models/usuario.model';

@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.component.html',
  styleUrls: ['./perfil.component.css']
})
export class PerfilComponent implements OnInit {

  usuarios: UsuarioModel[] = [];

  constructor( private auth: AuthService) { }

  ngOnInit(): void {

    this.auth.getUsuario()
      .subscribe( resp => {
        console.log(resp);
        this.usuarios = resp;
      })

  }
  

}
