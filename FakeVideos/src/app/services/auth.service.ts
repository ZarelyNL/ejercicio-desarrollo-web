import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import createAuth0Client from '@auth0/auth0-spa-js';
import Auth0Client from '@auth0/auth0-spa-js/dist/typings/Auth0Client';
import { from, of, Observable, BehaviorSubject, combineLatest, throwError } from 'rxjs';
import { tap, catchError, concatMap, shareReplay } from 'rxjs/operators';
import { Router } from '@angular/router';

import { map } from 'rxjs/operators';
import { UsuarioModel } from '../models/usuario.model';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  /*auth0Client$ = (from(
    createAuth0Client({
      domain: "dev-vglk8oh7.us.auth0.com",
      client_id: "xN7w6HLv3qHTt7DzFGccN0nwqI2XEvpV",
      redirect_uri: `${window.location.origin}`
    })
  ) as Observable<Auth0Client>).pipe(
    shareReplay(1), 
    catchError(err => throwError(err)) 
  );
  isAuthenticated$ = this.auth0Client$.pipe(
    concatMap((client: Auth0Client) => from(client.isAuthenticated())),
    tap(res => this.loggedIn = res)
  );
  handleRedirectCallback$ = this.auth0Client$.pipe(
    concatMap((client: Auth0Client) => from(client.handleRedirectCallback()))
  );
  
  private userProfileSubject$ = new BehaviorSubject<any>(null);
  userProfile$ = this.userProfileSubject$.asObservable();
  
  loggedIn: boolean = null;*/

  userToken: string;
  booltoken: boolean;

  private url = 'https://identitytoolkit.googleapis.com/v1';
  private apikey = 'AIzaSyB0LKyBZ8QRAWxpZmqR_3OJ7s9lvh0jc6g';
  private urlBD = 'https://fakevid.firebaseio.com';

  //Nuevo usuario
  //https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=[API_KEY]

  //login
  //https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=[API_KEY]


  constructor( private http: HttpClient, private router: Router ) { 

    this.leerToken();
    
    //this.localAuthSetup();
    //this.handleAuthCallback();

  }

  /*getUser$(options?): Observable<any> {
    return this.auth0Client$.pipe(
      concatMap((client: Auth0Client) => from(client.getUser(options))),
      tap(user => this.userProfileSubject$.next(user))
    );
  }

  private localAuthSetup() {
    
    const checkAuth$ = this.isAuthenticated$.pipe(
      concatMap((loggedIn: boolean) => {
        if (loggedIn) {
          
          return this.getUser$();
        }
        
        return of(loggedIn);
      })
    );
    checkAuth$.subscribe();
  }

  login_auth0(redirectPath: string = '/') {
    
    this.auth0Client$.subscribe((client: Auth0Client) => {
      
      client.loginWithRedirect({
        redirect_uri: `${window.location.origin}`,
        appState: { target: redirectPath }
      });
    });
  }

  private handleAuthCallback() {
    
    const params = window.location.search;
    if (params.includes('code=') && params.includes('state=')) {
      let targetRoute: string; 
      const authComplete$ = this.handleRedirectCallback$.pipe(
        
        tap(cbRes => {
          
          targetRoute = cbRes.appState && cbRes.appState.target ? cbRes.appState.target : '/';
        }),
        concatMap(() => {
          
          return combineLatest([
            this.getUser$(),
            this.isAuthenticated$
          ]);
        })
      );
      
      authComplete$.subscribe(([user, loggedIn]) => {
        
        this.router.navigate([targetRoute]);
      });
    }
  }

  logout_auth0() {
    // Ensure Auth0 client instance exists
    this.auth0Client$.subscribe((client: Auth0Client) => {
      // Call method to log out
      client.logout({
        client_id: "xN7w6HLv3qHTt7DzFGccN0nwqI2XEvpV",
        returnTo: `${window.location.origin}`
      });
    });
  }*/


////
  logout() {
    localStorage.removeItem('token');
    this.booltoken = false;
    console.log(this.booltoken);
    console.log("1");
  }

  login( usuario: UsuarioModel ) {

    const authData = {
      ...usuario,
      returnSecureToken: true
    };
    console.log("2");
    this.booltoken = true;
    console.log(this.booltoken);

    return this.http.post(
      `${ this.url }/accounts:signInWithPassword?key=${ this.apikey }`,
      authData
    ).pipe(
      map( resp => {
        this.guardarToken( resp['idToken'] );        
        console.log(resp);
        return resp;
      })
    );

  }

  nuevoUsuario( usuario: UsuarioModel ) {

    const authData = {
      ...usuario,
      returnSecureToken: true
    };

    return this.http.post(
      `${ this.url }/accounts:signUp?key=${this.apikey}`,
      authData
    ).pipe(
      map( resp => {
        this.guardarToken( resp['idToken'] );
        this.booltoken = true;
        console.log(this.booltoken);
        console.log("13");
        return resp;
      })
    );
    

  }


  private guardarToken( idToken: string ) {

    this.userToken = idToken;
    localStorage.setItem('token', idToken);
    console.log("4");

    let hoy = new Date();
    hoy.setSeconds( 120 );
    console.log("5");

    localStorage.setItem('expira', hoy.getTime().toString() );
    console.log("6");


  }

  leerToken() {

    if ( localStorage.getItem('token') ) {
      this.userToken = localStorage.getItem('token');
      console.log( "7" );
      console.log(this.userToken);
    } else {
      this.userToken = '';
      console.log( "8" );
    }

    return this.userToken;

  }


  estaAutenticado(): boolean {

    if ( this.userToken.length < 2 ) {
      console.log("9");
      return false;
    }

    const expira = Number(localStorage.getItem('expira'));
    const expiraDate = new Date();
    expiraDate.setTime(expira);

    console.log("10");

    if ( expiraDate > new Date() ) {
      console.log("11");
      return true;
    } else {
      console.log("12");
      return false;
    }

  }

  CrearUsuario( usuario: UsuarioModel){
    return this.http.post(`${ this.urlBD }/usuarios.json`, usuario)
        .pipe(
          map( (resp: any ) => {
            usuario.id = resp.name;
            return usuario;
          })
        );
  }

  getUsuario(){
    return this.http.get( `${ this.urlBD }/usuarios.json`)
        .pipe(
          map( this.Arreglo )
        );
  }

  private Arreglo( usuariosObj: Object){
    
    const usuarios: UsuarioModel[] = [];

    if ( usuariosObj === null ) { return [];}

    Object.keys(usuariosObj ).forEach( key => {
      const usuario: UsuarioModel = usuariosObj[key];
      usuario.id = key;
      usuarios.push( usuario );
    });
    return usuarios;
  }

}
